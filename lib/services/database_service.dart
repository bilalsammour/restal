import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:restal/models/map_modal.dart';

class DatabaseService {
  final String collectionPath;

  final _instance = FirebaseFirestore.instance;
  CollectionReference _collection;

  DatabaseService(this.collectionPath) {
    _collection = _instance.collection(collectionPath);
  }

  Future<Map<String, dynamic>> get(String id) async {
    try {
      var doc = await _collection.doc(id).get();

      return doc.data();
    } catch (_) {
      return {};
    }
  }

  Stream<DocumentSnapshot> getAsStream(String id) {
    try {
      return _collection.doc(id).snapshots();
    } catch (_) {
      return null;
    }
  }

  Stream<List<Map<String, dynamic>>> getStream() {
    try {
      var snapshots = _collection.snapshots();

      return snapshots.map(_mapListFromSnapshot);
    } catch (_) {
      return null;
    }
  }

  Stream<QuerySnapshot> getSnapshots() {
    try {
      return _collection.snapshots();
    } catch (_) {
      return null;
    }
  }

  List<Map<String, dynamic>> _mapListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((doc) {
      return doc.data();
    }).toList();
  }

  Future set(String id, MapModal data) async {
    await _collection.doc(id).set(data.toMap());
  }

  Future add(MapModal data) async {
    await _collection.add(data.toMap());
  }

  Future delete(String id) async {
    await _collection.doc(id).delete();
  }
}

class EnumManager {
  static T getEnumFromStringWithDefault<T>(
      Iterable<T> values, String value, T defaultValue) {
    var castedValue = getEnumFromString<T>(values, value);

    if (castedValue == null) castedValue = defaultValue;

    return castedValue;
  }

  static T getEnumFromString<T>(Iterable<T> values, String value) {
    return values.firstWhere((type) => type.toString().split(".").last == value,
        orElse: () => null);
  }
}

import 'package:flutter/material.dart';

class DialogsManager {
  static Future showOkDialog({
    @required BuildContext context,
    @required String message,
  }) async {
    await showDialog<void>(
      context: context,
      builder: (context) {
        return _buildTitleMessageAlertDialog(
          context,
          message,
        );
      },
    );
  }

  static AlertDialog _buildTitleMessageAlertDialog(
    BuildContext context,
    String message,
  ) {
    return AlertDialog(
      title: Text('ResTal'),
      content: SingleChildScrollView(
        child: Text(message),
      ),
      actions: <Widget>[
        _buildOkButton(context),
      ],
    );
  }

  static Widget _buildOkButton(BuildContext context) {
    return FlatButton(
      child: Text('OK'),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }
}

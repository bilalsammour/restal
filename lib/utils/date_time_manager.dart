import 'package:intl/intl.dart';

class DateTimeManager {
  static final DateTime minDate = new DateTime(1900);
  static final DateTime maxDate = new DateTime(2900);

  static DateTime parseDataTimeWithMinimum(String formattedString) {
    return parseDataTimeWithDefault(formattedString, new DateTime(1900));
  }

  static DateTime parseDataTimeWithDefault(
      String formattedString, DateTime defaultDateTime) {
    try {
      return DateTime.parse(formattedString);
    } catch (_) {
      return defaultDateTime;
    }
  }

  static DateTime parseDataTime(String formattedString) {
    return DateTime.parse(formattedString);
  }

  static bool isWithin(DateTime value, DateTime from, DateTime to) {
    return !(value.isBefore(from) || value.isAfter(to));
  }

  static String formatDate(DateTime value, String format) {
    return DateFormat(format).format(value);
  }
}

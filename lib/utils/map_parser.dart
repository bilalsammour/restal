import 'date_time_manager.dart';
import 'enum_manager.dart';

class MapParser {
  static bool parseBoolWithDefault(
      Map<String, dynamic> json, String key, bool defaultValue) {
    try {
      return parseDynamicWithDefault(json, key, defaultValue);
    } catch (_) {
      return defaultValue;
    }
  }

  static int parseBool(Map<String, dynamic> json, String key) {
    return parseDynamic(json, key);
  }

  static T parseEnumWithDefault<T>(Map<String, dynamic> json, String key,
      Iterable<T> values, T defaultValue) {
    try {
      String source = parseString(json, key);

      return EnumManager.getEnumFromString(values, source);
    } catch (_) {
      return defaultValue;
    }
  }

  static T parseEnum<T>(
      Map<String, dynamic> json, String key, Iterable<T> values) {
    String source = parseString(json, key);

    return EnumManager.getEnumFromString(values, source);
  }

  static T parseNumericEnum<T>(
      Map<String, dynamic> json, String key, Iterable<T> values) {
    int source = parseInt(json, key);

    return values.toList()[source];
  }

  static T parseNumericEnumWithDefault<T>(Map<String, dynamic> json, String key,
      Iterable<T> values, T defaultValue) {
    try {
      int source = parseInt(json, key);

      return values.toList()[source];
    } catch (_) {
      return defaultValue;
    }
  }

  static int parseIntWithDefault(
      Map<String, dynamic> json, String key, int defaultValue) {
    try {
      dynamic value = parseDynamicWithDefault(json, key, defaultValue);

      if (value is int)
        return value;
      else if (value is double)
        return value.toInt();
      else
        return defaultValue;
    } catch (_) {
      return defaultValue;
    }
  }

  static int parseInt(Map<String, dynamic> json, String key) {
    return parseDynamic(json, key);
  }

  static double parseDoubleWithDefault(
      Map<String, dynamic> json, String key, double defaultValue) {
    try {
      dynamic value = parseDynamicWithDefault(json, key, defaultValue);

      if (value is double)
        return value;
      else if (value is int)
        return value.toDouble();
      else
        return defaultValue;
    } catch (_) {
      return defaultValue;
    }
  }

  static double parseDouble(Map<String, dynamic> json, String key) {
    return parseDynamic(json, key);
  }

  static DateTime parseDateTimeWithDefault(
      Map<String, dynamic> json, String key, DateTime defaultValue) {
    try {
      String source =
          parseStringWithDefault(json, key, defaultValue.toString());

      return DateTimeManager.parseDataTimeWithMinimum(source);
    } catch (_) {
      return defaultValue;
    }
  }

  static DateTime parseDateTime(Map<String, dynamic> json, String key) {
    String source = parseString(json, key);

    return DateTimeManager.parseDataTimeWithMinimum(source);
  }

  static String parseStringWithDefault(
      Map<String, dynamic> json, String key, String defaultValue) {
    try {
      return parseDynamicWithDefault(json, key, defaultValue);
    } catch (_) {
      return defaultValue;
    }
  }

  static String parseString(Map<String, dynamic> json, String key) {
    return parseDynamic(json, key);
  }

  static dynamic parseDynamicWithDefault(
      Map<String, dynamic> json, String key, dynamic defaultValue) {
    if (json.containsKey(key))
      return parseDynamic(json, key) ?? defaultValue;
    else
      return defaultValue;
  }

  static dynamic parseDynamic(Map<String, dynamic> json, String key) {
    return json[key];
  }

  static bool hasKey(Map<String, dynamic> json, String key) {
    return json.containsKey(key);
  }

  static bool hasValue(Map<String, dynamic> json, String key) {
    return hasKey(json, key) && json[key] != null;
  }
}

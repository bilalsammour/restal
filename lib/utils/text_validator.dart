class TextValidator {
  static const String _EMPTY_ERROR = 'Should not be empty';

  static String validateName(String value) {
    if (value == null || value == '') {
      return _EMPTY_ERROR;
    }

    return null;
  }

  static String validatePersonName(String value) {
    if (value == null || value == '') {
      return _EMPTY_ERROR;
    }

    if (!isValidRegex(value, '^([a-zA-Z]{3,30}\s*)+\$')) {
      return 'Not valid name';
    }

    return null;
  }

  static String validateEmail(String value) {
    if (value == null || value == '') {
      return _EMPTY_ERROR;
    }

    if (!isValidRegex(value,
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")) {
      return 'Not valid email';
    }

    return null;
  }

  static String validatePassword(String value) {
    if (value == null || value == '') {
      return _EMPTY_ERROR;
    }

    if (value.length < 6) {
      return 'Short password';
    }

    return null;
  }

  static bool isValidRegex(String value, String regex) {
    RegExp regExp = new RegExp(regex);

    return regExp.hasMatch(value);
  }
}

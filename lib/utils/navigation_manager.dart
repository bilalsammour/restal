import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class NavigationManager {
  static Future push(BuildContext context, Widget page) async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => page,
      ),
    );
  }

  static Future pushNamedNoBack(BuildContext context, String routeName,
      {Object arguments}) async {
    return await Navigator.pushReplacementNamed(context, routeName,
        arguments: arguments);
  }

  static Future pushNamedWithoutBack(BuildContext context, String routeName,
      {Object arguments}) async {
    return await Navigator.pushNamedAndRemoveUntil(
        context, routeName, (v) => false,
        arguments: arguments);
  }

  static Future pushClearBack(BuildContext context, Widget page) async {
    await Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => page),
      (v) => false,
    );
  }

  static void pop(BuildContext context) {
    Navigator.pop(context);
  }

  static void popWithReturn(BuildContext context, Object value) {
    Navigator.pop(context, value);
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:restal/models/restaurant_model.dart';
import 'package:restal/models/rating_model.dart';
import 'package:restal/services/database_service.dart';
import 'package:restal/services/users_service.dart';

import 'base_controller.dart';

class RestaurantController extends BaseController {
  static const String _COLLECTION_NAME = 'restaurants';

  String _currentId;
  RestaurantModel _currentRestaurantModel;

  final UsersService _usersService = new UsersService();
  final DatabaseService _databaseService =
      new DatabaseService(_COLLECTION_NAME);

  String get currentUserId => _usersService.getCurrentUserId();

  Stream<List<RestaurantModel>> getRestaurantsStream() {
    return _databaseService.getSnapshots().map(_restaurantsSnapshotToList);
  }

  List<RestaurantModel> _restaurantsSnapshotToList(QuerySnapshot snapshot) {
    var list = snapshot.docs.map((doc) {
      return RestaurantModel.fromMap(doc.id, doc.data());
    }).toList();

    list.sort();

    return list;
  }

  Future add(RestaurantModel restaurantModel) async {
    changeBusy(true);

    try {
      await _databaseService.add(restaurantModel);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }

  Stream<RestaurantModel> readCurrentRestaurantStream() {
    return _databaseService
        .getAsStream(_currentId)
        .map((doc) => RestaurantModel.fromMap(doc.id, doc.data()));
  }

  Future<RestaurantModel> getItem(String id) async {
    var map = await _databaseService.get(id);

    return RestaurantModel.fromMap(id, map);
  }

  Future edit(RestaurantModel restaurantModel) async {
    changeBusy(true);

    try {
      await _databaseService.set(restaurantModel.id, restaurantModel);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }

  Future delete(RestaurantModel restaurantModel) async {
    changeBusy(true);

    try {
      await _databaseService.delete(restaurantModel.id);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }

  void selectCurrentId(String id) {
    _currentId = id;

    notifyListeners();
  }

  String getCurrentId() {
    return _currentId;
  }

  void selectCurrentItem(RestaurantModel restaurantModel) {
    _currentRestaurantModel = restaurantModel;

    notifyListeners();
  }

  RestaurantModel getCurrentItem() {
    return _currentRestaurantModel;
  }

  RatingModel getCurrentRatingItem() {
    var currentItem = getCurrentItem();
    var ratingList = currentItem.ratingList;

    return ratingList.firstWhere(
      (element) => element.userId == currentUserId,
      orElse: () => new RatingModel(),
    );
  }

  Future rate(RatingModel ratingModel) async {
    changeBusy(true);

    try {
      await _tryRate(ratingModel);
    } catch (_) {} finally {
      changeBusy(false);
    }
  }

  Future _tryRate(RatingModel ratingModel) async {
    ratingModel.userId = currentUserId;

    var currentItem = getCurrentItem();
    var ratingList = currentItem.ratingList;

    var foundItem = ratingList.firstWhere(
      (element) => element.userId == currentUserId,
      orElse: () => null,
    );

    if (foundItem == null) {
      ratingList.add(ratingModel);
    } else {
      foundItem.edit(ratingModel);
    }

    await _databaseService.set(currentItem.id, currentItem);
  }
}

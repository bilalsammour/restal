import 'package:firebase_auth/firebase_auth.dart';
import 'package:restal/models/user_model.dart';
import 'package:restal/services/database_service.dart';
import 'package:restal/services/users_service.dart';

import 'base_controller.dart';

class UserController extends BaseController {
  static const String _COLLECTION_NAME = 'users';

  String currentUserId;
  bool isAdminUser = false;

  final UsersService _usersService = new UsersService();
  final DatabaseService _databaseService =
      new DatabaseService(_COLLECTION_NAME);

  bool hasToken() => _usersService.hasToken();

  Future<String> login(String email, String password) async {
    changeBusy(true);

    try {
      var userCredential = await _usersService.signInWithEmailAndPassword(
          email: email, password: password);
      var firebaseUser = userCredential.user;

      currentUserId = userCredential.user.uid;

      var data = await _databaseService.get(firebaseUser.uid);

      var userModel = UserModel.fromMap(data);

      isAdminUser = userModel.isAdmin;

      return null;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return 'No user found for that email.';
      } else if (e.code == 'wrong-password') {
        return 'Wrong password provided for that user.';
      } else {
        return e.message;
      }
    } finally {
      changeBusy(false);
    }
  }

  Future<String> register(String email, String password, bool isAdmin) async {
    changeBusy(true);

    try {
      var userCredential = await _usersService.createUserWithEmailAndPassword(
          email: email, password: password);
      var firebaseUser = userCredential.user;

      var userModel = UserModel(isAdmin: isAdmin);

      await _databaseService.set(firebaseUser.uid, userModel);

      currentUserId = firebaseUser.uid;
      isAdminUser = isAdmin;

      return null;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return 'The password provided is too weak.';
      } else if (e.code == 'email-already-in-use') {
        return 'The account already exists for that email.';
      } else {
        return e.message;
      }
    } finally {
      changeBusy(false);
    }
  }

  Future logout() async {
    try {
      await _usersService.signOut();

      currentUserId = null;
      isAdminUser = false;
    } catch (_) {}
  }
}

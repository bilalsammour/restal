import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';

import 'controllers/restaurant_controller.dart';
import 'controllers/user_controller.dart';
import 'view/pages/home_page.dart';
import 'view/shared_widgets/full_progress.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UserController()),
        ChangeNotifierProvider(create: (_) => RestaurantController()),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: doFuture(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'ResTal',
            theme: ThemeData(
              primarySwatch: Colors.red,
              scaffoldBackgroundColor: Colors.grey.shade300,
              buttonColor: Colors.orange,
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
            home: HomePage(),
          );
        }

        return FullProgress();
      },
    );
  }

  Future doFuture() async {
    await Firebase.initializeApp();
  }
}

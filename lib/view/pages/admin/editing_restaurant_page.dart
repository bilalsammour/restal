import 'package:flutter/material.dart';
import 'package:restal/controllers/restaurant_controller.dart';
import 'package:restal/models/restaurant_model.dart';
import 'package:restal/utils/app_spaces.dart';
import 'package:restal/utils/navigation_manager.dart';
import 'package:restal/utils/text_validator.dart';
import 'package:restal/view/pages/admin/rates_page.dart';
import 'package:restal/view/shared_widgets/actions_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:restal/view/shared_widgets/busy_progress.dart';
import 'package:restal/view/shared_widgets/dismissible_keyboard.dart';
import 'package:restal/view/shared_widgets/modal_bottom_sheet_manager.dart';

class EditingRestaurantPage extends StatefulWidget {
  EditingRestaurantPage({Key key}) : super(key: key);

  @override
  _EditingRestaurantPageState createState() => _EditingRestaurantPageState();
}

class _EditingRestaurantPageState extends State<EditingRestaurantPage> {
  RestaurantController _controller;

  final _formKey = GlobalKey<FormState>();

  RestaurantModel _currentRestaurant;

  final TextEditingController _titleTextController =
      new TextEditingController();

  @override
  void initState() {
    super.initState();

    _controller = context.read<RestaurantController>();

    _currentRestaurant = _controller.getCurrentItem();

    _setValues();
  }

  void _setValues() {
    _titleTextController.text = _currentRestaurant.title;
  }

  @override
  Widget build(BuildContext context) {
    return DismissibleKeyboard(
      context: context,
      child: Scaffold(
        appBar: ActionsAppBar(title: 'Edit Restaurant'),
        body: _buildUi(),
      ),
    );
  }

  Widget _buildUi() {
    return Container(
      padding: EdgeInsets.all(AppSpaces.MAIN_SPACE),
      child: Stack(
        children: [
          SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: _buildFormContent(),
            ),
          ),
          Consumer<RestaurantController>(
            builder: (BuildContext context, RestaurantController value,
                    Widget child) =>
                BusyProgress(busy: value.busy),
          ),
        ],
      ),
    );
  }

  Widget _buildFormContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        TextFormField(
          decoration: InputDecoration(
            labelText: 'Restaurant Name',
          ),
          controller: _titleTextController,
          keyboardType: TextInputType.name,
          validator: (value) => TextValidator.validateName(value),
        ),
        SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
        RaisedButton(
          child: Text(
            'Save',
            style: Theme.of(context).textTheme.button,
          ),
          onPressed: () async {
            await _submitForm();
          },
        ),
        SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE * 2),
        RaisedButton(
          child: Text(
            'Show And Edit Rates',
            style: Theme.of(context).textTheme.button,
          ),
          onPressed: () async {
            await NavigationManager.push(context, RatesPage());
          },
        ),
      ],
    );
  }

  Future _submitForm() async {
    if (_formKey.currentState.validate()) {
      _currentRestaurant.title = _titleTextController.text;

      await _controller.edit(_currentRestaurant);

      ModalBottomSheetManager.show(context, 'Saved successfully!');
    }
  }
}

import 'package:flutter/material.dart';
import 'package:restal/controllers/restaurant_controller.dart';
import 'package:restal/models/restaurant_model.dart';
import 'package:restal/utils/app_spaces.dart';
import 'package:restal/utils/navigation_manager.dart';
import 'package:restal/view/shared_widgets/actions_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:restal/view/shared_widgets/busy_progress.dart';
import 'package:restal/view/shared_widgets/center_progress.dart';
import 'package:restal/view/shared_widgets/no_items.dart';

import 'adding_restaurant_page.dart';
import 'editing_restaurant_page.dart';

class AdminRestaurantsPage extends StatefulWidget {
  AdminRestaurantsPage({Key key}) : super(key: key);

  @override
  _AdminRestaurantsPageState createState() => _AdminRestaurantsPageState();
}

class _AdminRestaurantsPageState extends State<AdminRestaurantsPage> {
  RestaurantController _controller;

  @override
  void initState() {
    super.initState();

    _controller = context.read<RestaurantController>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ActionsAppBar(title: 'Admin Restaurants List'),
      body: StreamBuilder(
        stream: _controller.getRestaurantsStream(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            var list = snapshot.data;

            return _buildOkWidget(list);
          } else {
            return CenterProgress();
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await NavigationManager.push(context, AddingRestaurantPage());
        },
        tooltip: 'Add',
        child: Icon(Icons.add),
      ),
    );
  }

  Widget _buildOkWidget(List<RestaurantModel> list) {
    return Stack(
      children: [
        list.isEmpty
            ? NoItems()
            : ListView.builder(
                itemCount: list.length,
                itemBuilder: (context, index) {
                  var item = list[index];

                  return _buildListItem(item);
                },
              ),
        Consumer<RestaurantController>(
          builder: (BuildContext context, RestaurantController value,
                  Widget child) =>
              BusyProgress(busy: value.busy),
        ),
      ],
    );
  }

  Widget _buildListItem(RestaurantModel item) {
    return Card(
      margin: EdgeInsets.symmetric(
        horizontal: AppSpaces.HALF_SPACE,
        vertical: AppSpaces.QUARTER_SPACE,
      ),
      child: ListTile(
        title: Text(
          item.title,
          style: Theme.of(context).textTheme.bodyText1,
        ),
        subtitle: Text(
          item.formatedRatesNumber,
          style: Theme.of(context).textTheme.caption,
        ),
        leading: CircleAvatar(
          backgroundColor: Theme.of(context).primaryColor,
          child: Text('${item.getAverageRating()}'),
        ),
        trailing: IconButton(
          icon: Icon(
            Icons.delete,
          ),
          onPressed: () async {
            await _controller.delete(item);
          },
        ),
        onTap: () async {
          _controller.selectCurrentId(item.id);
          _controller.selectCurrentItem(item);

          await NavigationManager.push(context, EditingRestaurantPage());
        },
      ),
    );
  }
}

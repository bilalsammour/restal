import 'package:flutter/material.dart';
import 'package:restal/controllers/restaurant_controller.dart';
import 'package:restal/models/rating_model.dart';
import 'package:restal/models/restaurant_model.dart';
import 'package:restal/utils/app_spaces.dart';
import 'package:restal/utils/navigation_manager.dart';
import 'package:restal/view/pages/regular/restaurant_rating_page.dart';
import 'package:restal/view/shared_widgets/actions_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:restal/view/shared_widgets/center_progress.dart';
import 'package:restal/view/shared_widgets/no_items.dart';

class RatesPage extends StatefulWidget {
  RatesPage({Key key}) : super(key: key);

  @override
  _RatesPageState createState() => _RatesPageState();
}

class _RatesPageState extends State<RatesPage> {
  RestaurantController _controller;

  @override
  void initState() {
    super.initState();

    _controller = context.read<RestaurantController>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ActionsAppBar(title: 'Rates'),
      body: StreamBuilder(
        stream: _controller.readCurrentRestaurantStream(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return _buildOkWidget(snapshot.data);
          } else {
            return CenterProgress();
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await NavigationManager.push(context, RestaurantRatingPage());
        },
        tooltip: 'Rate',
        child: Icon(Icons.rate_review),
      ),
    );
  }

  Widget _buildOkWidget(RestaurantModel restaurantModel) {
    var list = restaurantModel.ratingList;

    return list.isEmpty
        ? NoItems()
        : ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
              var item = list[index];

              return _buildListItem(item);
            },
          );
  }

  Widget _buildListItem(RatingModel item) {
    return Card(
      margin: EdgeInsets.symmetric(
        horizontal: AppSpaces.HALF_SPACE,
        vertical: AppSpaces.QUARTER_SPACE,
      ),
      child: ListTile(
        title: Text(
          item.comment,
          style: Theme.of(context).textTheme.bodyText1,
        ),
        subtitle: Text(
          item.lastVisitDate.toString(),
          style: Theme.of(context).textTheme.caption,
        ),
        leading: CircleAvatar(
          backgroundColor: Theme.of(context).primaryColor,
          child: Text('${item.rating}'),
        ),
        trailing: IconButton(
          icon: Icon(
            Icons.delete,
          ),
          onPressed: () async {
            //await _controller.delete(item);
          },
        ),
        onTap: () async {
          //_controller.selectCurrentId(item.id);
          //_controller.selectCurrentItem(item);

          //await NavigationManager.push(context, EditingRestaurantPage());
        },
      ),
    );
  }
}

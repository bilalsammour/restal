import 'package:flutter/material.dart';
import 'package:restal/controllers/user_controller.dart';
import 'package:restal/utils/app_spaces.dart';
import 'package:restal/utils/dialogs_manager.dart';
import 'package:restal/utils/navigation_manager.dart';
import 'package:restal/utils/text_validator.dart';
import 'package:provider/provider.dart';
import 'package:restal/view/shared_widgets/busy_progress.dart';
import 'package:restal/view/shared_widgets/dismissible_keyboard.dart';

import '../admin/admin_restaurants_page.dart';
import '../regular/restaurants_page.dart';

class LoginRegistrationPage extends StatefulWidget {
  final bool isRegistration;

  LoginRegistrationPage({
    Key key,
    this.isRegistration = false,
  }) : super(key: key);

  @override
  _LoginRegistrationPageState createState() => _LoginRegistrationPageState();
}

class _LoginRegistrationPageState extends State<LoginRegistrationPage> {
  UserController _controller;

  final _formKey = GlobalKey<FormState>();

  final TextEditingController _emailController = new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();
  bool _isAdmin = false;

  @override
  void initState() {
    super.initState();

    _controller = context.read<UserController>();

    _controller.logout().then((value) {});
  }

  @override
  Widget build(BuildContext context) {
    return DismissibleKeyboard(
      context: context,
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.isRegistration ? 'Register' : 'Login'),
        ),
        body: _buildUi(),
      ),
    );
  }

  Widget _buildUi() {
    return Container(
      padding: EdgeInsets.all(AppSpaces.MAIN_SPACE),
      child: Stack(
        children: [
          SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: _builInputs() + _buildButtons(),
              ),
            ),
          ),
          Consumer<UserController>(
            builder:
                (BuildContext context, UserController value, Widget child) =>
                    BusyProgress(busy: value.busy),
          ),
        ],
      ),
    );
  }

  List<Widget> _builInputs() {
    return [
      TextFormField(
        decoration: InputDecoration(
          labelText: 'Email',
        ),
        controller: _emailController,
        keyboardType: TextInputType.emailAddress,
        validator: (value) => TextValidator.validateEmail(value),
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      TextFormField(
        decoration: InputDecoration(
          labelText: 'Password',
        ),
        controller: _passwordController,
        keyboardType: TextInputType.text,
        obscureText: true,
        validator: (value) => TextValidator.validatePassword(value),
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
    ];
  }

  List<Widget> _buildButtons() {
    return widget.isRegistration
        ? _buildRegistrationButtons()
        : _buildLoginButtons();
  }

  List<Widget> _buildLoginButtons() {
    return [
      RaisedButton(
        child: Text(
          'Login',
          style: Theme.of(context).textTheme.button,
        ),
        onPressed: () async {
          await login();
        },
      ),
      FlatButton(
        child: Text(
          'Dont have an account? Register',
          style: Theme.of(context).textTheme.button.apply(
                fontStyle: FontStyle.italic,
              ),
        ),
        onPressed: () async {
          await _navigateToMe(true);
        },
      )
    ];
  }

  List<Widget> _buildRegistrationButtons() {
    return [
      SwitchListTile(
        title: Text('Is admin?'),
        value: _isAdmin,
        onChanged: (value) {
          setState(() {
            _isAdmin = value;
          });
        },
      ),
      SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
      RaisedButton(
        child: Text('Register'),
        onPressed: () async {
          await register();
        },
      ),
      FlatButton(
        child: Text('Have an account? Login'),
        onPressed: () async {
          await _navigateToMe(false);
        },
      )
    ];
  }

  Future _navigateToMe(bool isRegistration) async {
    await NavigationManager.pushClearBack(
      context,
      LoginRegistrationPage(
        isRegistration: isRegistration,
      ),
    );
  }

  Future login() async {
    if (!_checkForm()) {
      return;
    }

    String error = await _controller.login(
      _emailController.text,
      _passwordController.text,
    );

    if (error != null) {
      await DialogsManager.showOkDialog(context: context, message: error);
      return;
    }

    await _navigateToMain(_controller.isAdminUser);
  }

  Future register() async {
    if (!_checkForm()) {
      return;
    }

    String error = await _controller.register(
        _emailController.text, _passwordController.text, _isAdmin);

    if (error != null) {
      await DialogsManager.showOkDialog(context: context, message: error);
      return;
    }

    await _navigateToMain(_controller.isAdminUser);
  }

  Future _navigateToMain(bool isAdmin) async {
    var page = isAdmin ? AdminRestaurantsPage() : RestaurantsPage();

    await NavigationManager.pushClearBack(
      context,
      page,
    );
  }

  bool _checkForm() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      return true;
    } else {
      return false;
    }
  }
}

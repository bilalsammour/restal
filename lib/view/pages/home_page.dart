import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:restal/controllers/user_controller.dart';

import 'admin/admin_restaurants_page.dart';
import 'regular/restaurants_page.dart';
import 'user/login_registration_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  UserController _controller;

  @override
  void initState() {
    super.initState();

    _controller = context.read<UserController>();
  }

  @override
  Widget build(BuildContext context) {
    if (_controller.hasToken()) {
      if (_controller.isAdminUser) {
        return AdminRestaurantsPage();
      } else {
        return RestaurantsPage();
      }
    } else {
      return LoginRegistrationPage();
    }
  }
}

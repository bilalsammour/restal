import 'package:flutter/material.dart';
import 'package:restal/controllers/restaurant_controller.dart';
import 'package:restal/models/restaurant_model.dart';
import 'package:restal/utils/app_spaces.dart';
import 'package:restal/utils/navigation_manager.dart';
import 'package:restal/view/shared_widgets/actions_app_bar.dart';
import 'package:provider/provider.dart';
import 'package:restal/view/shared_widgets/center_progress.dart';
import 'package:restal/view/shared_widgets/no_items.dart';

import 'restaurant_detail_page.dart';

class RestaurantsPage extends StatefulWidget {
  RestaurantsPage({Key key}) : super(key: key);

  @override
  _RestaurantsPageState createState() => _RestaurantsPageState();
}

class _RestaurantsPageState extends State<RestaurantsPage> {
  RestaurantController _controller;

  @override
  void initState() {
    super.initState();

    _controller = context.read<RestaurantController>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ActionsAppBar(title: 'Restaurants List'),
      body: StreamBuilder(
        stream: _controller.getRestaurantsStream(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            var list = snapshot.data;

            return _buildOkWidget(list);
          } else {
            return CenterProgress();
          }
        },
      ),
    );
  }

  Widget _buildOkWidget(List<RestaurantModel> list) {
    return list.isEmpty
        ? NoItems()
        : ListView.builder(
            itemCount: list.length,
            itemBuilder: (context, index) {
              var item = list[index];

              return _buildListItem(item);
            },
          );
  }

  Widget _buildListItem(RestaurantModel item) {
    return Card(
      margin: EdgeInsets.symmetric(
        horizontal: AppSpaces.HALF_SPACE,
        vertical: AppSpaces.QUARTER_SPACE,
      ),
      child: ListTile(
        title: Text(
          item.title,
          style: Theme.of(context).textTheme.bodyText1,
        ),
        subtitle: Text(
          item.formatedRatesNumber,
          style: Theme.of(context).textTheme.caption,
        ),
        leading: CircleAvatar(
          backgroundColor: Theme.of(context).primaryColor,
          child: Text('${item.getAverageRating()}'),
        ),
        onTap: () async {
          _controller.selectCurrentId(item.id);
          _controller.selectCurrentItem(item);

          await NavigationManager.push(context, RestaurantDetailPage());
        },
      ),
    );
  }
}

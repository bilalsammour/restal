import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restal/controllers/restaurant_controller.dart';
import 'package:restal/models/rating_model.dart';
import 'package:restal/utils/app_spaces.dart';
import 'package:restal/view/shared_widgets/actions_app_bar.dart';
import 'package:restal/view/shared_widgets/busy_progress.dart';
import 'package:restal/view/shared_widgets/dismissible_keyboard.dart';
import 'package:restal/view/shared_widgets/modal_bottom_sheet_manager.dart';
import 'package:restal/view/shared_widgets/star_rating_widget.dart';
import 'package:provider/provider.dart';

class RestaurantRatingPage extends StatefulWidget {
  RestaurantRatingPage({Key key}) : super(key: key);

  @override
  _RestaurantRatingPageState createState() => _RestaurantRatingPageState();
}

class _RestaurantRatingPageState extends State<RestaurantRatingPage> {
  RestaurantController _controller;

  final _formKey = GlobalKey<FormState>();

  double _rating;
  final TextEditingController _commentTextController =
      new TextEditingController();
  DateTime _lastVisitDate;

  @override
  void initState() {
    super.initState();

    _controller = context.read<RestaurantController>();
  }

  @override
  Widget build(BuildContext context) {
    _fillDate();

    return DismissibleKeyboard(
      context: context,
      child: Scaffold(
        appBar: ActionsAppBar(title: _controller.getCurrentItem().title),
        body: Consumer<RestaurantController>(
          builder:
              (BuildContext context, RestaurantController value, Widget _) {
            return _buildUi();
          },
        ),
      ),
    );
  }

  void _fillDate() {
    var currentRatingItem = _controller.getCurrentRatingItem();

    _rating = currentRatingItem.rating;
    _commentTextController.text = currentRatingItem.comment;
    _lastVisitDate =
        DateTime.fromMillisecondsSinceEpoch(currentRatingItem.lastVisitDate);
  }

  Widget _buildUi() {
    return Container(
      padding: EdgeInsets.all(AppSpaces.MAIN_SPACE),
      child: Stack(
        children: [
          SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: _buildFormContent(),
            ),
          ),
          Consumer<RestaurantController>(
            builder: (BuildContext context, RestaurantController value,
                    Widget child) =>
                BusyProgress(busy: value.busy),
          ),
        ],
      ),
    );
  }

  Widget _buildFormContent() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        InputDecorator(
          decoration: InputDecoration(labelText: 'Rating'),
          child: StarRatingWidget(
            initialRating: _rating,
            onRatingUpdate: (rating) {
              _rating = rating;
            },
          ),
        ),
        SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
        InputDecorator(
          decoration: InputDecoration(labelText: 'Last Visit Date'),
          child: Container(
            height: 200.0,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.date,
              initialDateTime: _lastVisitDate,
              maximumDate: DateTime.now(),
              onDateTimeChanged: (value) async {
                if (value == null) return;

                _lastVisitDate = value;
              },
            ),
          ),
        ),
        SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
        TextFormField(
          decoration: InputDecoration(
            labelText: 'Comment',
          ),
          controller: _commentTextController,
          keyboardType: TextInputType.name,
        ),
        SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
        RaisedButton(
          child: Text(
            'Rate',
            style: Theme.of(context).textTheme.button,
          ),
          onPressed: () async {
            await _submitForm();
          },
        )
      ],
    );
  }

  Future _submitForm() async {
    if (_formKey.currentState.validate()) {
      await _controller.rate(RatingModel.fields(
        userId: _controller.currentUserId,
        rating: _rating,
        lastVisitDate: _lastVisitDate.millisecondsSinceEpoch,
        comment: _commentTextController.text,
      ));

      ModalBottomSheetManager.show(context, 'Thank you!');
    }
  }
}

import 'package:flutter/material.dart';
import 'package:restal/controllers/restaurant_controller.dart';
import 'package:restal/models/restaurant_model.dart';
import 'package:restal/utils/app_spaces.dart';
import 'package:restal/utils/navigation_manager.dart';
import 'package:restal/view/shared_widgets/actions_app_bar.dart';
import 'package:restal/view/shared_widgets/rating_widget.dart';
import 'package:restal/view/shared_widgets/star_rating_widget.dart';
import 'package:provider/provider.dart';

import 'restaurant_rating_page.dart';

class RestaurantDetailPage extends StatefulWidget {
  RestaurantDetailPage({Key key}) : super(key: key);

  @override
  _RestaurantDetailPageState createState() => _RestaurantDetailPageState();
}

class _RestaurantDetailPageState extends State<RestaurantDetailPage> {
  RestaurantController _controller;

  @override
  void initState() {
    super.initState();

    _controller = context.read<RestaurantController>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ActionsAppBar(title: _controller.getCurrentItem().title),
      body: Consumer<RestaurantController>(
        builder: (BuildContext context, RestaurantController value, Widget _) {
          return _buildUi();
        },
      ),
    );
  }

  Widget _buildUi() {
    var currentItem = _controller.getCurrentItem();

    return Container(
      padding: EdgeInsets.all(AppSpaces.MAIN_SPACE),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            _buildRatesSwitcherUi(currentItem),
            SizedBox(height: AppSpaces.DOUBLE_MAIN_SPACE),
            RaisedButton(
              child: Text(
                'Rate',
                style: Theme.of(context).textTheme.button,
              ),
              onPressed: () async {
                await NavigationManager.push(context, RestaurantRatingPage());
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRatesSwitcherUi(RestaurantModel currentItem) {
    if (currentItem.ratingList.isNotEmpty) {
      return _buildRatesUi(currentItem);
    } else {
      return _buildNoRatesUi(currentItem);
    }
  }

  Widget _buildRatesUi(RestaurantModel currentItem) {
    return Column(
      children: [
        Card(
          child: ListTile(
            contentPadding: EdgeInsets.symmetric(
              horizontal: AppSpaces.MAIN_SPACE,
              vertical: AppSpaces.HALF_SPACE,
            ),
            title: Text(
              'Average Rating',
              style: Theme.of(context).textTheme.headline5,
            ),
            subtitle: StarRatingWidget(
              initialRating: currentItem.getAverageRating(),
              readOnly: true,
            ),
          ),
        ),
        SizedBox(height: AppSpaces.HALF_SPACE),
        RatingWidget(
          title: 'Highest Rated Review',
          ratingModel: currentItem.getHighestRatingModel(),
        ),
        SizedBox(height: AppSpaces.HALF_SPACE),
        RatingWidget(
          title: 'Lowest Rated Review',
          ratingModel: currentItem.getLowestRatingModel(),
        ),
        SizedBox(height: AppSpaces.HALF_SPACE),
        RatingWidget(
          title: 'Latest Rated Review',
          ratingModel: currentItem.getLatesttRatingModel(),
          withComment: true,
        ),
      ],
    );
  }

  Widget _buildNoRatesUi(RestaurantModel currentItem) {
    return Center(
      child: Text(
        'No rates available',
        style: Theme.of(context).textTheme.headline4,
      ),
    );
  }
}

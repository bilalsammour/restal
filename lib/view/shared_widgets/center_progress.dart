import 'package:flutter/material.dart';

class CenterProgress extends StatelessWidget {
  CenterProgress({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(),
    );
  }
}

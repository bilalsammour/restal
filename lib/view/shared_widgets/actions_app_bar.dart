import 'package:flutter/material.dart';
import 'package:restal/utils/navigation_manager.dart';
import 'package:restal/view/pages/user/login_registration_page.dart';

class ActionsAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title;

  const ActionsAppBar({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title),
      actions: [
        IconButton(
          icon: Icon(Icons.power_settings_new),
          onPressed: () async {
            await _logout(context);
          },
        )
      ],
    );
  }

  Future _logout(BuildContext context) async {
    await NavigationManager.pushClearBack(context, LoginRegistrationPage());
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}

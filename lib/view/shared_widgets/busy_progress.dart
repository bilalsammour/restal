import 'package:flutter/material.dart';

import 'full_progress.dart';

class BusyProgress extends StatelessWidget {
  final bool busy;

  BusyProgress({
    Key key,
    @required this.busy,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return busy ? FullProgress() : SizedBox.shrink();
  }
}

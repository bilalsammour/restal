import 'package:flutter/material.dart';
import 'package:restal/models/rating_model.dart';
import 'package:restal/utils/app_spaces.dart';

import 'star_rating_widget.dart';

class RatingWidget extends StatelessWidget {
  final String title;
  final RatingModel ratingModel;
  final bool withComment;

  RatingWidget({
    Key key,
    @required this.title,
    @required this.ratingModel,
    this.withComment = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
          child: ListTile(
            contentPadding: EdgeInsets.symmetric(
              horizontal: AppSpaces.MAIN_SPACE,
              vertical: AppSpaces.HALF_SPACE,
            ),
            title: Text(
              title,
              style: Theme.of(context).textTheme.headline5,
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                StarRatingWidget(
                  initialRating: ratingModel.rating,
                  readOnly: true,
                ),
                withComment ? _buildCommentWidget(context) : SizedBox.shrink(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildCommentWidget(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: AppSpaces.HALF_SPACE),
        Text(
          ratingModel.comment,
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ],
    );
  }
}

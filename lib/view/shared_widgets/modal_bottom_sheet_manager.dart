import 'package:flutter/material.dart';
import 'package:restal/utils/app_spaces.dart';

class ModalBottomSheetManager {
  static void show(BuildContext context, String text) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
          padding: EdgeInsets.symmetric(
            vertical: AppSpaces.DOUBLE_MAIN_SPACE,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                text,
                style: Theme.of(context).textTheme.headline5,
              ),
            ],
          ),
        );
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'center_progress.dart';

class FullProgress extends StatelessWidget {
  FullProgress({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints.expand(),
      color: Theme.of(context).scaffoldBackgroundColor,
      child: CenterProgress(),
    );
  }
}

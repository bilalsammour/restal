import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:restal/utils/app_spaces.dart';

class StarRatingWidget extends StatefulWidget {
  final double initialRating;
  final bool readOnly;
  final ValueChanged<double> onRatingUpdate;

  StarRatingWidget({
    Key key,
    @required this.initialRating,
    this.readOnly = false,
    this.onRatingUpdate,
  }) : super(key: key);

  @override
  _StarRatingWidgetState createState() => _StarRatingWidgetState();
}

class _StarRatingWidgetState extends State<StarRatingWidget> {
  static const double _MIN = 1.0;
  static const int _MAX = 5;

  @override
  Widget build(BuildContext context) {
    return RatingBar(
      initialRating: widget.initialRating,
      ignoreGestures: widget.readOnly,
      minRating: _MIN,
      allowHalfRating: true,
      itemCount: _MAX,
      itemPadding: EdgeInsets.symmetric(horizontal: AppSpaces.HALF_SPACE),
      itemBuilder: (context, _) => Icon(
        Icons.star,
        color: Colors.amber,
      ),
      onRatingUpdate: (double value) {
        widget.onRatingUpdate?.call(value);
      },
    );
  }
}

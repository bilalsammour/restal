import 'package:flutter/material.dart';

class NoItems extends StatelessWidget {
  const NoItems({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'No items to show',
        style: Theme.of(context).textTheme.headline5,
      ),
    );
  }
}

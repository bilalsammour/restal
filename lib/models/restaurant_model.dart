import 'package:restal/utils/map_parser.dart';

import 'map_modal.dart';
import 'rating_model.dart';

class RestaurantModel implements MapModal, Comparable<RestaurantModel> {
  String id;
  String title;
  DateTime date;
  List<RatingModel> ratingList;

  RestaurantModel() {
    id = '';
    title = '';
    date = DateTime.now();
    ratingList = [];
  }

  RestaurantModel.fields({
    this.id,
    this.title,
  }) {
    date = DateTime.now();
    ratingList = [];
  }

  RestaurantModel.fromMap(String id, Map<String, dynamic> map) {
    this.id = id;

    title = MapParser.parseStringWithDefault(map, 'title', '');
    date = MapParser.parseDateTimeWithDefault(map, 'date', DateTime.now());

    ratingList = [];
    if (MapParser.hasValue(map, 'rating_list')) {
      MapParser.parseDynamic(map, 'rating_list').forEach((v) {
        ratingList.add(new RatingModel.fromMap('', v));
      });

      ratingList.sort((a, b) => b.compareTo(a));
    }
  }

  void edit(RestaurantModel restaurantModel) {
    title = restaurantModel.id;
    date = restaurantModel.date;
  }

  String get formatedRatesNumber {
    int number = ratesNumber;

    String s = number > 1 ? 's' : '';

    return '$number rate$s';
  }

  int get ratesNumber {
    return ratingList == null ? 0 : ratingList.length;
  }

  double getAverageRating() {
    try {
      double value =
          ratingList.map((element) => element.rating).reduce((a, b) => a + b) /
              ratingList.length;

      return value ?? 0.0;
    } catch (_) {
      return 0.0;
    }
  }

  RatingModel getHighestRatingModel() {
    return ratingList.reduce(
        (value, element) => value.rating >= element.rating ? value : element);
  }

  RatingModel getLowestRatingModel() {
    return ratingList.reduce(
        (value, element) => value.rating <= element.rating ? value : element);
  }

  RatingModel getLatesttRatingModel() {
    return ratingList.reduce((value, element) =>
        value.lastVisitDate.compareTo(element.lastVisitDate) >= 0
            ? value
            : element);
  }

  @override
  Map<String, dynamic> toMap() {
    List<Map<String, dynamic>> ratingListMaps = [];
    ratingList.forEach((element) {
      ratingListMaps.add(element.toMap());
    });

    return {
      'title': title,
      'date': date,
      'rating_list': ratingListMaps,
    };
  }

  @override
  int get hashCode => id.hashCode;

  bool operator ==(o) => id == o.id;

  @override
  toString() {
    return title;
  }

  @override
  int compareTo(RestaurantModel other) {
    return _compareToDescending(other);
  }

  int _compareToDescending(RestaurantModel other) {
    return other.getAverageRating().compareTo(getAverageRating());
  }
}

import 'package:restal/models/map_modal.dart';
import 'package:restal/utils/map_parser.dart';

class UserModel implements MapModal {
  String id;
  bool isAdmin;

  UserModel({
    this.id = '',
    this.isAdmin = false,
  });

  UserModel.fromMap(Map<String, dynamic> map) {
    isAdmin = MapParser.parseBoolWithDefault(map, 'is_admin', false);
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'is_admin': isAdmin,
    };
  }
}

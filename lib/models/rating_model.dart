import 'package:restal/utils/map_parser.dart';

import 'map_modal.dart';

class RatingModel implements MapModal, Comparable<RatingModel> {
  String id;
  String userId;
  double rating;
  int lastVisitDate;
  String comment;

  RatingModel() {
    id = '';
    userId = '';
    rating = 0.0;
    lastVisitDate = _getNow();
    comment = '';
  }

  RatingModel.fields({
    this.id,
    this.userId,
    this.rating,
    this.lastVisitDate,
    this.comment,
  });

  RatingModel.fromMap(String id, Map<String, dynamic> map) {
    this.id = id;

    userId = MapParser.parseStringWithDefault(map, 'user_id', '');
    rating = MapParser.parseDoubleWithDefault(map, 'rating', 0.0);
    lastVisitDate =
        MapParser.parseIntWithDefault(map, 'last_visit_date', _getNow());
    comment = MapParser.parseStringWithDefault(map, 'comment', '');
  }

  int _getNow() => (DateTime.now()).millisecondsSinceEpoch;

  void edit(RatingModel ratingModel) {
    rating = ratingModel.rating;
    lastVisitDate = ratingModel.lastVisitDate;
    comment = ratingModel.comment;
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      'user_id': userId,
      'rating': rating,
      'last_visit_date': lastVisitDate,
      'comment': comment,
    };
  }

  @override
  int compareTo(RatingModel other) {
    return lastVisitDate.compareTo(other.lastVisitDate);
  }
}

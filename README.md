# ResTal
An application to Review Restaurants for Toptal.

## Rules

### Admin User
Admin user is able to:

* See the list of the restaurants.
* Add restaurants.
* Edit restaurants.
* Delete restaurants.

### Regular User
Regular user is able to:

* See the list of the restaurants.
* Rate.
* Leave a comment.
* Rate and leave a comment for a restaurant once.
* When rating the same restaurant again, it will edit the rating, so it will be calculated one.

## Technical Aspects

### Platform
The app is a Flutter based project for Android and iOS, and can be ported to web and desktop (Windows, macOS, Linux) as well.

### State Management
The app uses [Provided](https://pub.dev/packages/provider) which is a wrapper around [InheritedWidget](https://api.flutter.dev/flutter/widgets/InheritedWidget-class.html).

### API
Firebase is used as the API layer for the project, used services:

* Firebase authentication.
* Cloud Firestore.

### Self-explanatory
No code documentation is used since the code themself is documented by their structure and naming convention.

## Users for Testing

### Admin Users

* admin1@email.com / paSSwrd123
* admin2@email.com / paSSwrd123

### Regular Users

* user1@email.com / paSSwrd123
* user2@email.com / paSSwrd123